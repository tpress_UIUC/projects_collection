Note: If you're reading this on my public repository, you will not find any code for this project here!
Please request access on GitLab if you wish to view any code. You may be expected to verify your identity.

ECE 391 is the operating systems course in the computer engineering curriculum at UIUC.
Topics covered in this course include:

--x86 assembly: review of basic constructs and structures, interfacing C to assembly, macros, stack frame and calling convention
--system software basics: resource management, virtualization, protection, system call interface, asynchronous and synchronous interactions
--simple data structures: queues, heaps, stacks, lists
--interrupts and exceptions: controlling generation and handling, chaining, cleanup code, interactions with device functionality, interrupt controllers
--synchronization: primitives, memory semantics, mutual exclusion, semaphores, scheduling, race conditions
--virtualization of the CPU: processes and scheduling
--I/O interface: file descriptors, buffering, control operations
--device programming: basic abstractions, character and block devices, device driver development process
--user-level programming interfaces for file and network I/O, relationship to kernel I/O abstractions
--virtualization of memory: hardware support and software abstractions
--signals: semantics, generation, and delivery
--file system abstractions and disk layout

See https://courses.engr.illinois.edu/ece391/fa2017/ for more info on the course.
The final project in this class (done in groups of 4) is to create an operating system largely from scratch.

An incomplete list of major tasks accomplished in this machine problem include:
--Creating an Interrupt Descriptor Table
--Setting up a virtualized Intel i8259A Programmable Interrupt Controler (PIC)
--Creating interrupt handlers for the Real Time Clock (RTC), and keyboard
--Setting up Paging
--Creating device drivers for the RTC, file system, and terminal
--Creating interrupt and system call entry routines
--Allowing execution of user level programs via the 'execute' and 'halt' system calls
--Enabling multiple terminals to be active simultaneously

Goals which we were unable to achieve include:
--Implementing a process scheduler
--Fixing a handful of bugs as detailed in the bug log

